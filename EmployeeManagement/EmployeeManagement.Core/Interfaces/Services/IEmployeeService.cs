﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Core.Models;
using Microsoft.AspNetCore.Http;

namespace EmployeeManagement.Core.Interfaces.Services
{
    public interface IEmployeeService
    {
        Task<List<Employee>> GetAllEmployeesAsync(bool includeTodos);
        Task<Employee> GetEmployeeByIdAsync(int employeeId, bool includeTodos);        
        Task CreateEmployeeAsync(Employee employee, IFormFile uploadedPhoto);
        Task UpdateEmployeeAsync(Employee employee, IFormFile uploadedPhoto);
        Task DeleteEmployeeAsync(int employeeId);
        Task ManageEmployeeTodosAsync(int employeeId, int[] todosIds);
    }
}
