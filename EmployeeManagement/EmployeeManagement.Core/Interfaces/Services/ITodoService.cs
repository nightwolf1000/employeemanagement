﻿using EmployeeManagement.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeManagement.Core.Interfaces.Services
{
    public interface ITodoService
    {
        Task<List<Todo>> GetAllTodosAsync(bool includeEmployees);
        Task<Todo> GetTodoByIdAsync(int todoId, bool includeEmployees);        
        Task CreateTodoAsync(Todo todo);
        Task UpdateTodoAsync(Todo todo);
        Task DeleteTodoAsync(int todoId);
        Task ManageEmployeesOnTodoAsync(int todoId, int[] employeesIds);
    }
}
