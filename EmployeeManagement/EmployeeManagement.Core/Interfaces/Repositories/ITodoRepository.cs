﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.Core.Interfaces.Repositories
{
    public interface ITodoRepository
    {
        Task<List<Todo>> GetAllTodosAsync();
        Task<Todo> GetTodoByIdAsync(int todoId);
        Task<Todo> GetTodoByIdWithEmployeeTodosAsync(int todoId);
        Task<Todo> GetTodoByIdWithEmployeesAsync(int todoId);
        Task<List<Todo>> GetAllTodosWithEmployeesAsync();
        Task CreateTodoAsync(Todo todo);
        void UpdateTodo(Todo todo);
        void DeleteTodo(Todo todo);
        Task<bool> TodoEntryExistsAsync(int todoId);
    }
}
