﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.Core.Interfaces.Repositories
{
    public interface IEmployeeRepository
    {
        Task<List<Employee>> GetAllEmployeesAsync();
        Task<Employee> GetEmployeeByIdAsync(int employeeId);
        Task<Employee> GetEmployeeByIdWithEmployeeTodosAsync(int employeeId);
        Task<Employee> GetEmployeeByIdWithTodosAsync(int employeeId);
        Task<List<Employee>> GetAllEmployeesWithTodosAsync();
        Task CreateEmployeeAsync(Employee employee);
        void UpdateEmployee(Employee employee);
        void DeleteEmployee(Employee employee);
        Task<bool> EmployeeEntryExistsAsync(int employeeId);
    }
}
