﻿using System;
using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.Repositories;

namespace EmployeeManagement.Core.Interfaces.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IEmployeeRepository EmployeeRepository { get; }
        ITodoRepository TodoRepository { get; }
        Task CompleteAsync();
    }
}
