﻿namespace EmployeeManagement.Core.Models
{
    public class EmployeeTodo
    {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public int TodoId { get; set; }
        public Todo Todo { get; set; }
    }
}
