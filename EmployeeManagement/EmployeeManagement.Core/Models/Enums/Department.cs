﻿namespace EmployeeManagement.Core.Models.Enums
{
    public enum Department
    {
        Accounting,
        Marketing,
        HR
    }
}
