﻿using EmployeeManagement.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.Core.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [Required]
        public Department? Department { get; set; }
        [Required]
        [MaxLength(50)]
        public string Position { get; set; }
        [Required]
        public int? Age { get; set; }
        [Required]
        public DateTime EmploymentDate { get; set; }
        public string PhotoPath { get; set; }
        public List<EmployeeTodo> EmployeeTodos { get; set; }

        public Employee()
        {
            EmployeeTodos = new List<EmployeeTodo>();
        }
    }
}
