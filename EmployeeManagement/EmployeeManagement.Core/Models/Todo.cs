﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.Core.Models
{
    public class Todo
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public List<EmployeeTodo> EmployeeTodos { get; set; }
        public Todo()
        {
            EmployeeTodos = new List<EmployeeTodo>();
        }
    }
}
