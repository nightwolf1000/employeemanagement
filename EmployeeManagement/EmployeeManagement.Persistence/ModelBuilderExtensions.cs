﻿using EmployeeManagement.Core.Models;
using EmployeeManagement.Core.Models.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeManagement.Persistence
{
    public static class ModelBuilderExtensions
    {
        public static void SeedData(this ModelBuilder builder)
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    Name = "John",
                    Email = "cooljonny@gmail.com",
                    Department = Department.Marketing,
                    Position = "Manager",
                    Age = 34,
                    EmploymentDate = new DateTime(2017, 05, 11),
                    PhotoPath = "seed_8a32b60a-7374-42fb-b87b-ff29438412b4_image1.jpg"
                },

                new Employee
                {
                    Id = 2,
                    Name = "Tom",
                    Email = "tombrown@yahoo.com",
                    Department = Department.Marketing,
                    Position = "Manager",
                    Age = 27,
                    EmploymentDate = new DateTime(2014, 12, 3),
                    PhotoPath = "seed_2a36223f-ee7f-4fd3-afc9-35be755adc47_image2.jpg"
                },

                new Employee
                {
                    Id = 3,
                    Name = "Anna",
                    Email = "annawhite23@example.com",
                    Department = Department.Accounting,
                    Position = "Accountant",
                    Age = 29,
                    EmploymentDate = new DateTime(2016, 04, 23),
                    PhotoPath = "seed_eccb94af-f032-459a-a6a6-1dc55ca2f8a7_image3.jpg"
                }
            };

            List<Todo> todos = new List<Todo>
            {
                new Todo { Id = 1, Name = "Implement the sales plan" },
                new Todo { Id = 2, Name = "Pay salary"},
                new Todo { Id = 3, Name = "Hire personnel" },
                new Todo { Id = 4, Name = "Prepare the annual report" },
            };

            List<EmployeeTodo> employeeTodos = new List<EmployeeTodo>
            {
                new EmployeeTodo { EmployeeId = 1, TodoId = 1 },
                new EmployeeTodo { EmployeeId = 2, TodoId = 1 },
                new EmployeeTodo { EmployeeId = 3, TodoId = 2  },
                new EmployeeTodo { EmployeeId = 1, TodoId = 4 },
                new EmployeeTodo { EmployeeId = 3, TodoId = 4 }
            };

            builder.Entity<Employee>().HasData(employees);
            builder.Entity<Todo>().HasData(todos);
            builder.Entity<EmployeeTodo>().HasData(employeeTodos);
        }
    }
}
