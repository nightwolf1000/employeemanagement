﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.Repositories;
using EmployeeManagement.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EmployeeManagement.Persistence.Repositories
{
    public class EFTodoRepository : ITodoRepository
    {
        private readonly ApplicationDbContext _context;

        public EFTodoRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public async Task<List<Todo>> GetAllTodosAsync()
        {
            var todos = await _context.Todos.ToListAsync();
            return todos;
        }

        public async Task<Todo> GetTodoByIdAsync(int todoId)
        {
            Todo todo = await _context.Todos
                .FirstOrDefaultAsync(t => t.Id == todoId);

            return todo;
        }

        public async Task<Todo> GetTodoByIdWithEmployeeTodosAsync(int todoId)
        {
            Todo todo = await _context.Todos
                .Include(t => t.EmployeeTodos)
                .FirstOrDefaultAsync(t => t.Id == todoId);

            return todo;
        }

        public async Task<Todo> GetTodoByIdWithEmployeesAsync(int todoId)
        {
            Todo todo = await _context.Todos
                .Include(t => t.EmployeeTodos)
                .ThenInclude(et => et.Employee)                
                .FirstOrDefaultAsync(t => t.Id == todoId);

            return todo;
        }

        public async Task<List<Todo>> GetAllTodosWithEmployeesAsync()
        {
            var todos = await _context.Todos
                .Include(t => t.EmployeeTodos)
                .ThenInclude(et => et.Employee)                
                .ToListAsync();

            return todos;
        }

        public async Task CreateTodoAsync(Todo todo)
        {
            await _context.Todos.AddAsync(todo);            
        }

        public void UpdateTodo(Todo todo)
        {
            EntityEntry<Todo> dbEntry = _context.Todos.Attach(todo);
            dbEntry.State = EntityState.Modified;            
        }

        public void DeleteTodo(Todo todo)
        {
            _context.Todos.Remove(todo);
        }

        public async Task<bool> TodoEntryExistsAsync(int todoId)
        {
            Todo todo = await _context.Todos
                .AsNoTracking()
                .FirstOrDefaultAsync(t => t.Id == todoId);

            return todo != null; 
        }
    }
}
