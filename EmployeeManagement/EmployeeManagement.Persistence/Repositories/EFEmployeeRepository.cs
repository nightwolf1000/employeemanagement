﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.Repositories;
using EmployeeManagement.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EmployeeManagement.Persistence.Repositories
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _context;

        public EFEmployeeRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public async Task<List<Employee>> GetAllEmployeesAsync()
        {
            var employees = await _context.Employees.ToListAsync();
            return employees;
        }

        public async Task<Employee> GetEmployeeByIdAsync(int employeeId)
        {
            Employee employee = await _context.Employees                
                .FirstOrDefaultAsync(e => e.Id == employeeId);

            return employee;
        }

        public async Task<Employee> GetEmployeeByIdWithEmployeeTodosAsync(int employeeId)
        {
            Employee employee = await _context.Employees
                .Include(e => e.EmployeeTodos)
                .FirstOrDefaultAsync(e => e.Id == employeeId);

            return employee;
        }

        public async Task<Employee> GetEmployeeByIdWithTodosAsync(int employeeId)
        {
            Employee employee = await _context.Employees
                .Include(e => e.EmployeeTodos)
                .ThenInclude(et => et.Todo)                
                .FirstOrDefaultAsync(e => e.Id == employeeId);

            return employee;
        }

        public async Task<List<Employee>> GetAllEmployeesWithTodosAsync()
        {
            var employees = await _context.Employees
                .Include(e => e.EmployeeTodos)
                .ThenInclude(et => et.Todo)
                .ToListAsync();

            return employees;
        }

        public async Task CreateEmployeeAsync(Employee employee)
        {
            await _context.Employees.AddAsync(employee);                
        }

        public void UpdateEmployee(Employee employee)
        {
            _context.Employees.Update(employee);              
        }

        public void DeleteEmployee(Employee employee)
        {
            _context.Employees.Remove(employee);
        }

        public async Task<bool> EmployeeEntryExistsAsync(int employeeId)
        {
            Employee employee = await _context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == employeeId);

            return employee != null;
        }
    }
}
