﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace employeemanagement.persistence.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    Department = table.Column<int>(nullable: false),
                    Position = table.Column<string>(maxLength: 50, nullable: false),
                    Age = table.Column<int>(nullable: false),
                    EmploymentDate = table.Column<DateTime>(nullable: false),
                    PhotoPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Todos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Todos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTodo",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(nullable: false),
                    TodoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTodo", x => new { x.EmployeeId, x.TodoId });
                    table.ForeignKey(
                        name: "FK_EmployeeTodo_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeTodo_Todos_TodoId",
                        column: x => x.TodoId,
                        principalTable: "Todos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Age", "Department", "Email", "EmploymentDate", "Name", "PhotoPath", "Position" },
                values: new object[,]
                {
                    { 1, 34, 1, "cooljonny@gmail.com", new DateTime(2017, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "seed_8a32b60a-7374-42fb-b87b-ff29438412b4_image1.jpg", "Manager" },
                    { 2, 27, 1, "tombrown@yahoo.com", new DateTime(2014, 12, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", "seed_2a36223f-ee7f-4fd3-afc9-35be755adc47_image2.jpg", "Manager" },
                    { 3, 29, 0, "annawhite23@example.com", new DateTime(2016, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Anna", "seed_eccb94af-f032-459a-a6a6-1dc55ca2f8a7_image3.jpg", "Accountant" }
                });

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Implement the sales plan" },
                    { 2, "Pay salary" },
                    { 3, "Hire personnel" },
                    { 4, "Prepare the annual report" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeTodo",
                columns: new[] { "EmployeeId", "TodoId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 1, 4 },
                    { 3, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTodo_TodoId",
                table: "EmployeeTodo",
                column: "TodoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeTodo");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Todos");
        }
    }
}
