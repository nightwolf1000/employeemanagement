﻿using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.UnitOfWork;
using EmployeeManagement.Core.Interfaces.Repositories;
using EmployeeManagement.Persistence.Repositories;

namespace EmployeeManagement.Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private IEmployeeRepository _employeeRepository;
        private ITodoRepository _todoRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            this._context = context;
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                _employeeRepository = _employeeRepository ?? new EFEmployeeRepository(_context);
                return _employeeRepository;                
            }
        }

        public ITodoRepository TodoRepository
        {
            get
            {
                _todoRepository = _todoRepository ?? new EFTodoRepository(_context);
                return _todoRepository;                
            }

        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose() => _context.Dispose();
    }
}
