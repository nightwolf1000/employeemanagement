﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }        
        public DbSet<Todo> Todos { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<EmployeeTodo>().HasKey(eat => new { eat.EmployeeId, eat.TodoId });
            builder.SeedData();
        }
    }
}
