﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.Services;
using EmployeeManagement.Core.Interfaces.UnitOfWork;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Infrastructure;

namespace EmployeeManagement.ApplicationServices
{
    public class TodoService : ITodoService
    {
        private readonly IUnitOfWork _unitOfWork;        

        public TodoService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;            
        }

        public async Task<List<Todo>> GetAllTodosAsync(bool includeEmployees)
        {
            if (includeEmployees)
            {
                return await _unitOfWork.TodoRepository.GetAllTodosWithEmployeesAsync();
            }
            return await _unitOfWork.TodoRepository.GetAllTodosAsync();
        }

        public async Task<Todo> GetTodoByIdAsync(int todoId, bool includeEmployees)
        {
            Todo todo = null;
            
            if (includeEmployees)
            {
                todo = await _unitOfWork.TodoRepository.GetTodoByIdWithEmployeesAsync(todoId);
            }
            else
            {
                todo = await _unitOfWork.TodoRepository.GetTodoByIdAsync(todoId);
            }
            
            if (todo == null)
            {
                throw new ItemNotFoundException($"Todo with Id = { todoId } cannot be found");
            }
            return todo;
        }

        public async Task CreateTodoAsync(Todo todo)
        {
            await _unitOfWork.TodoRepository.CreateTodoAsync(todo);
            await _unitOfWork.CompleteAsync();            
        }

        public async Task UpdateTodoAsync(Todo todo)
        {
            if (await _unitOfWork.TodoRepository.TodoEntryExistsAsync(todo.Id))
            {
                _unitOfWork.TodoRepository.UpdateTodo(todo);
                await _unitOfWork.CompleteAsync();                
            }
            else
            {
                throw new ItemNotFoundException($"Todo with Id = { todo.Id } cannot be found");
            }
        }

        public async Task DeleteTodoAsync(int todoId)
        {
            Todo dbEntry = await _unitOfWork.TodoRepository.GetTodoByIdAsync(todoId);

            if (dbEntry == null)
            {
                throw new ItemNotFoundException($"Todo with Id = { todoId } cannot be found");
            }
            _unitOfWork.TodoRepository.DeleteTodo(dbEntry);
            await _unitOfWork.CompleteAsync();
        }

        public async Task ManageEmployeesOnTodoAsync(int todoId, int[] employeesIds)
        {
            Todo dbEntry = await _unitOfWork.TodoRepository
                .GetTodoByIdWithEmployeeTodosAsync(todoId);

            if (dbEntry == null)
            {
                throw new ItemNotFoundException($"Todo with Id = { todoId } cannot be found");
            }

            List<Employee> employees = await _unitOfWork.EmployeeRepository.GetAllEmployeesAsync();
            dbEntry.EmployeeTodos.Clear();

            foreach (Employee employee in employees)
            {
                if (employeesIds.Contains(employee.Id))
                {
                    dbEntry.EmployeeTodos.Add(new EmployeeTodo
                    {
                        EmployeeId = employee.Id,
                        TodoId = dbEntry.Id
                    });
                }
            }
            await _unitOfWork.CompleteAsync();            
        }
    }
}
