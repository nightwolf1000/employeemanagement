﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Core.Interfaces.Services;
using EmployeeManagement.Core.Interfaces.UnitOfWork;
using EmployeeManagement.Core.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using EmployeeManagement.Infrastructure;

namespace EmployeeManagement.ApplicationServices
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _environment;

        public EmployeeService(IUnitOfWork unitOfWork, IWebHostEnvironment environment)
        {
            this._unitOfWork = unitOfWork;
            this._environment = environment;
        }

        public async Task<List<Employee>> GetAllEmployeesAsync(bool includeTodos)
        {
            if (includeTodos)
            {
                return await _unitOfWork.EmployeeRepository
                    .GetAllEmployeesWithTodosAsync();
            }
            return await _unitOfWork.EmployeeRepository
                .GetAllEmployeesAsync();                
        }

        public async Task<Employee> GetEmployeeByIdAsync(int employeeId, bool includeTodos)
        {
            Employee employee = null;
            
            if (includeTodos)
            {
                employee = await _unitOfWork.EmployeeRepository
                    .GetEmployeeByIdWithTodosAsync(employeeId);
            }
            else
            {
                employee = await _unitOfWork.EmployeeRepository
                .GetEmployeeByIdAsync(employeeId);
            }
            
            if (employee == null)
            {
                throw new ItemNotFoundException($"Employee with Id = { employeeId } cannot be found");
            }
            return employee;
        }

        public async Task CreateEmployeeAsync(Employee employee, IFormFile uploadedPhoto)
        {
            if (uploadedPhoto != null)
            {
                string uniqueFileName = await ProcessUploadedPhotoAsync(uploadedPhoto);
                employee.PhotoPath = uniqueFileName;
            }          
            await _unitOfWork.EmployeeRepository.CreateEmployeeAsync(employee);
            await _unitOfWork.CompleteAsync();            
        }

        public async Task UpdateEmployeeAsync(Employee employee, IFormFile uploadedPhoto)
        {
            if (await _unitOfWork.EmployeeRepository.EmployeeEntryExistsAsync(employee.Id))
            {
                if (uploadedPhoto != null)
                {
                    DeleteExistingPhoto(employee);
                    string uniqueFileName = await ProcessUploadedPhotoAsync(uploadedPhoto);
                    employee.PhotoPath = uniqueFileName;
                }
                _unitOfWork.EmployeeRepository.UpdateEmployee(employee);
                await _unitOfWork.CompleteAsync();                
            }
            else
            {
                throw new ItemNotFoundException($"Employee with Id = { employee.Id } cannot be found");
            }            
        }

        public async Task DeleteEmployeeAsync(int employeeId)
        {
            Employee dbEntry = await _unitOfWork.EmployeeRepository
                .GetEmployeeByIdAsync(employeeId);

            if (dbEntry == null)
            {
                throw new ItemNotFoundException($"Employee with Id = { employeeId } cannot be found");
            }

            DeleteExistingPhoto(dbEntry);
            _unitOfWork.EmployeeRepository.DeleteEmployee(dbEntry);
            await _unitOfWork.CompleteAsync();            
        }

        public async Task ManageEmployeeTodosAsync(int employeeId, int[] todosIds)
        {
            Employee dbEntry = await _unitOfWork.EmployeeRepository
                .GetEmployeeByIdWithEmployeeTodosAsync(employeeId);

            if (dbEntry == null)
            {
                throw new ItemNotFoundException($"Employee with Id = { employeeId } cannot be found");
            }

            List<Todo> todos = await _unitOfWork.TodoRepository.GetAllTodosAsync();
            dbEntry.EmployeeTodos.Clear();

            foreach (Todo todo in todos)
            {
                if (todosIds.Contains(todo.Id))
                {
                    dbEntry.EmployeeTodos.Add(new EmployeeTodo
                    {
                        EmployeeId = dbEntry.Id,
                        TodoId = todo.Id
                    });
                }
            }
            await _unitOfWork.CompleteAsync();            
        }

        private async Task<string> ProcessUploadedPhotoAsync(IFormFile uploadedPhoto)
        {
            string uploadsFolder = Path.Combine(_environment.WebRootPath, "images");
            string uniqueFileName = Guid.NewGuid().ToString() + "_" + uploadedPhoto.FileName;
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await uploadedPhoto.CopyToAsync(fileStream);
            }
            return uniqueFileName;
        }

        private void DeleteExistingPhoto(Employee employee)
        {
            if (employee.PhotoPath != null && !employee.PhotoPath.StartsWith("seed"))
            {
                string filePath = Path.Combine(_environment.WebRootPath, "images", employee.PhotoPath);
                System.IO.File.Delete(filePath);
            }
        }
    }
}
