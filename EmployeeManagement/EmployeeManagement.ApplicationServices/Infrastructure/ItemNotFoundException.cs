﻿using System;
using System.Runtime.Serialization;

namespace EmployeeManagement.Infrastructure
{
    [Serializable]
    public class ItemNotFoundException : ApplicationException
    {
        public ItemNotFoundException() { }
        public ItemNotFoundException(string message) { }
        public ItemNotFoundException(string message, Exception innerException)
            : base(message, innerException) { }
        protected ItemNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }       
    }
}
