﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Core.Interfaces.Services;
using EmployeeManagement.Infrastructure;

namespace EmployeeManagement.WebUI.Controllers
{    
    
    public class HomeController : Controller
    {
        private readonly IEmployeeService _employeeService;        

        public HomeController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;            
        }

        public async Task<IActionResult> Index()
        {
            List<Employee> employees = await _employeeService.GetAllEmployeesAsync(false);
            return View(employees);
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                Employee employee = await _employeeService.GetEmployeeByIdAsync(id, true);
                return View(employee);

            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }            
        }
    }
}
