﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Core.Interfaces.Services;
using AutoMapper;
using EmployeeManagement.WebUI.ViewModels;
using EmployeeManagement.Infrastructure;

namespace EmployeeManagement.WebUI.Controllers
{    
    public class AdminController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly ITodoService _todoService;
        private readonly IMapper _mapper;

        public AdminController(IEmployeeService employeeService, ITodoService todoService, IMapper mapper)
        {
            this._employeeService = employeeService;
            this._todoService = todoService;
            this._mapper = mapper;
        }
        
        [HttpGet]
        public async Task<IActionResult> ListEmployees()
        {
            List<Employee> employees = await _employeeService.GetAllEmployeesAsync(false);
            return View(employees);
        }

        [HttpGet]
        public IActionResult CreateEmployee()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee(CreateEmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                Employee employee = _mapper.Map<CreateEmployeeViewModel, Employee>(model);
                await _employeeService.CreateEmployeeAsync(employee, model.Photo);
                TempData["message"] = $"Employee {employee.Name} " +
                    $"was succesfully created!";

                return RedirectToAction(nameof(ListEmployees));
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditEmployee(int employeeId)
        {
            try
            {
                Employee employee = await _employeeService.GetEmployeeByIdAsync(employeeId, true);
                EditEmployeeViewModel model = _mapper.Map<Employee, EditEmployeeViewModel>(employee);
                return View(model);
            }
            catch(ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }            
        }

        [HttpPost]
        public async Task<IActionResult> EditEmployee(EditEmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Employee employee = _mapper.Map<EditEmployeeViewModel, Employee>(model);
                    await _employeeService.UpdateEmployeeAsync(employee, model.Photo);
                    TempData["message"] = $"Employee with ID = {employee.Id} " +
                            $"was succesfully updated!";
                    return RedirectToAction(nameof(ListEmployees));
                }
                catch(ItemNotFoundException exc)
                {
                    Response.StatusCode = 404;
                    ViewBag.ErrorMessage = exc.Message;
                    return View("NotFound");
                }
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditEmployeeTodos(int employeeId)
        {
            try
            {
                Employee employee = await _employeeService.GetEmployeeByIdAsync(employeeId, true);
                EditEmployeeTodosViewModel model = _mapper.Map<Employee, EditEmployeeTodosViewModel>(employee);
                ViewBag.AllTodos = await _todoService.GetAllTodosAsync(false);
                return View(model);
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }            
        }
        
        [HttpPost]
        public async Task<IActionResult> EditEmployeeTodos(int employeeId, string employeeName, int[] todosIds)
        {
            try
            {
                await _employeeService.ManageEmployeeTodosAsync(employeeId, todosIds);
                TempData["message"] = $"{employeeName}'s todos " +
                            $"were succesfully managed!";
                return RedirectToAction(nameof(EditEmployee), new { employeeId = employeeId });
            }
            catch(ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }            
        }

        [HttpPost]
        public async Task<IActionResult> DeleteEmployee(int employeeId)
        {
            try
            {
                await _employeeService.DeleteEmployeeAsync(employeeId);
                TempData["message"] = $"Employee with ID={employeeId} " +
                        $"was succesfully deleted!";

                return RedirectToAction(nameof(ListEmployees));
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }
            
        }

        [HttpGet]
        public async Task<IActionResult> ListTodos()
        {
            List<Todo> todos = await _todoService.GetAllTodosAsync(false);
            return View(todos);
        }

        [HttpGet]
        public IActionResult CreateTodo()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateTodo(CreateTodoViewModel model)
        {
            if (ModelState.IsValid)
            {
                Todo todo = _mapper.Map<CreateTodoViewModel, Todo>(model);
                await _todoService.CreateTodoAsync(todo);
                TempData["message"] = $"Todo {todo.Name} " +
                    $"was succesfully created!";

                return RedirectToAction(nameof(ListTodos));
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditTodo(int todoId)
        {
            try
            {
                Todo todo = await _todoService.GetTodoByIdAsync(todoId, true);
                EditTodoViewModel model = _mapper.Map<Todo, EditTodoViewModel>(todo);
                return View(model);
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditTodo(EditTodoViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Todo todo = _mapper.Map<EditTodoViewModel, Todo>(model);
                    await _todoService.UpdateTodoAsync(todo);
                    TempData["message"] = $"Todo with ID = {todo.Id} " +
                            $"was succesfully updated!";
                    return RedirectToAction(nameof(ListTodos));
                }
                catch (ItemNotFoundException exc)
                {
                    Response.StatusCode = 404;
                    ViewBag.ErrorMessage = exc.Message;
                    return View("NotFound");
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditTodoEmployees(int todoId)
        {
            try
            {
                Todo todo = await _todoService.GetTodoByIdAsync(todoId, true);
                EditTodoEmployeesViewModel model = _mapper.Map<Todo, EditTodoEmployeesViewModel>(todo);
                ViewBag.AllEmployees = await _employeeService.GetAllEmployeesAsync(false);
                return View(model);
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditTodoEmployees(int todoId, string todoName, int[] employeesIds)
        {
            try
            {
                await _todoService.ManageEmployeesOnTodoAsync(todoId, employeesIds);
                TempData["message"] = $"Employees assigned to todo \"{todoName}\" " +
                            $"were succesfully managed!";
                return RedirectToAction(nameof(EditTodo), new { todoId = todoId });
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteTodo(int todoId)
        {
            try
            {
                await _todoService.DeleteTodoAsync(todoId);
                TempData["message"] = $"Todo with ID={todoId} " +
                        $"was succesfully deleted!";

                return RedirectToAction(nameof(ListTodos));
            }
            catch (ItemNotFoundException exc)
            {
                Response.StatusCode = 404;
                ViewBag.ErrorMessage = exc.Message;
                return View("NotFound");
            }
        }
    }
}
