﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Diagnostics;

namespace EmployeeManagement.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> logger;
        public ErrorController(ILogger<ErrorController> logger)
        {
            this.logger = logger;
        }

        [Route("Error/{statusCode}")]
        public ViewResult HttpStatusCodeHandler(int statusCode)
        {
            IStatusCodeReExecuteFeature statusCodeResult =
                    HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            logger.LogWarning($"{statusCode} error occured. Path = " +
                $"{statusCodeResult.OriginalPath} and QueryString = " +
                $"{statusCodeResult.OriginalQueryString}");

            if (statusCode == 401)
            {
                return View("Unauthorized");
            }
            else if (statusCode == 403)
            {
                return View("Forbidden");
            }
            else
            {
                ViewBag.ErrorMessage = "Sorry, the resource you " +
                    "requested could not be found";
                return View("NotFound");
            }
        }

        [Route("Error")]
        public ViewResult Error()
        {
            IExceptionHandlerPathFeature exceptionHandlerPathFeature =
                    HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            logger.LogError($"The path {exceptionHandlerPathFeature.Path} " +
                $"threw an exception {exceptionHandlerPathFeature.Error}");

            return View("Error");
        }
    }
}
