﻿using AutoMapper;
using EmployeeManagement.WebUI.ViewModels;
using EmployeeManagement.Core.Models;
using System.Linq;

namespace EmployeeManagement.WebUI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateEmployeeViewModel, Employee>();
            CreateMap<Employee, EditEmployeeViewModel>()
                .ForMember(evm => evm.Todos, opt => opt.MapFrom(e => e.EmployeeTodos.Select(et => et.Todo)));
            CreateMap<EditEmployeeViewModel, Employee>();
            CreateMap<Employee, EditEmployeeTodosViewModel>()
                .ForMember(evm => evm.EmployeeId, opt => opt.MapFrom(e => e.Id))
                .ForMember(evm => evm.EmployeeName, opt => opt.MapFrom(e => e.Name))
                .ForMember(evm => evm.Todos, opt => opt.MapFrom(e => e.EmployeeTodos.Select(et => et.Todo)));

            CreateMap<CreateTodoViewModel, Todo>();
            CreateMap<Todo, EditTodoViewModel>()
                .ForMember(tvm => tvm.Employees, opt => opt.MapFrom(t => t.EmployeeTodos.Select(et => et.Employee)));
            CreateMap<EditTodoViewModel, Todo>();
            CreateMap<Todo, EditTodoEmployeesViewModel>()
                .ForMember(tvm => tvm.TodoId, opt => opt.MapFrom(t => t.Id))
                .ForMember(tvm => tvm.TodoName, opt => opt.MapFrom(t => t.Name))
                .ForMember(tvm => tvm.Employees, opt => opt.MapFrom(t => t.EmployeeTodos.Select(et => et.Employee)));
        }        
    }
}
