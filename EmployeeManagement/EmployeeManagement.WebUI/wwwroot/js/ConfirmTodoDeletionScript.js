﻿$(document).ready(function () {

    var dataId, dataName;

    $(".deletionButton").click(function () {

        dataId = $(this).data("id");
        dataName = $(this).data("name");

        $("#confirmDeletionModal .modal-body")
            .text("Are you sure you want to delete todo \""
                + dataName + "\" with ID = " + dataId + "?");

        $("#confirmDeletionModal").modal("show");
    });

    $("#delete").click(function () {

        $("#deletionForm_" + dataId).submit();
        $("#confirmDeletionModal").modal("hide");
    });

});