﻿using System.ComponentModel.DataAnnotations;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.WebUI.ViewModels
{
    public class CreateTodoViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string Name { get; set; }
    }
}
