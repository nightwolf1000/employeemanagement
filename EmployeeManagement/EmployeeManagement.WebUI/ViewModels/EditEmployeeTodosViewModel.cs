﻿using System.Collections.Generic;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.WebUI.ViewModels
{
    public class EditEmployeeTodosViewModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public IEnumerable<Todo> Todos { get; set; }        
    }
}
