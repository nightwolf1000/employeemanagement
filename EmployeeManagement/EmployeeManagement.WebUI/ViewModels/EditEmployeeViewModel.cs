﻿using System.Collections.Generic;
using System.Linq;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.WebUI.ViewModels
{
    public class EditEmployeeViewModel : CreateEmployeeViewModel
    {
        public IEnumerable<Todo> Todos { get; set; }
    }
}
