﻿using EmployeeManagement.Core.Models.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using EmployeeManagement.WebUI.Utils;

namespace EmployeeManagement.WebUI.ViewModels
{
    public class CreateEmployeeViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string Name { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Email cannot exceed 50 characters")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
            ErrorMessage = "Invalid Email Format")]
        [Display(Name = "Office Email")]
        public string Email { get; set; }
        [Required]
        public Department? Department { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Position cannot exceed 50 characters")]
        public string Position { get; set; }
        [Required]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Age must be an integer number")]
        [Range(0, 99, ErrorMessage = "Age must be in the range 0-99")]
        public int? Age { get; set; }
        [Required]
        [Display(Name = "Employment Date")]        
        public DateTime EmploymentDate { get; set; }
        public string PhotoPath { get; set; }
        [MaxFileSize(1048576)]
        [AllowedExtensions(new string[] { ".jpeg", ".jpg", ".png" })]
        public IFormFile Photo { get; set; }
    }
}
