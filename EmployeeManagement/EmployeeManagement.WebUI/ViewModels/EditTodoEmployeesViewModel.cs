﻿using System.Collections.Generic;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.WebUI.ViewModels
{
    public class EditTodoEmployeesViewModel
    {
        public int TodoId { get; set; }
        public string TodoName { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
}
